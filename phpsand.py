import os

filename = '/tmp/tmp_sample.php'
if not os.path.exists(filename):
	os.system('touch '+filename)
	file = open(filename, "r+")
	file.write('<?php\n\n')
	file.close()

try:
	while True:
		os.system('nano '+filename)
		os.system('clear')
		os.system('php '+filename)
		try:
			key = input('\n\nPress enter to edit or ctrl+c to end')
		except SyntaxError:
			print('error')
except KeyboardInterrupt:
	os.remove(filename)
	print('\nbye\n')
	exit()
